var experienceModel = require('../model/experienceModel.js');

module.exports = {

    listExperience : function (callback) {
        experienceModel.selectExperience(0,"",function (err, result) {
                callback(result);
        });
    },

    createExperience : function (data, callback) {
        experienceModel.insertExperience(0, data, function (err, result) {
            callback(result);
        });
    }
};