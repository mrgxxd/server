var userModel = require('../model/userModel.js');

module.exports = {
    userLogin : function (data,callback) {
        userModel.selectUser(0,data, function (err,result) {
            var code = result.code;
            if(code == 0){
                var accountId = result.data[0].account_id;
                userModel.updateUser(0,accountId,data,function (updateErr,updateResult) {
                        userModel.selectUser(0,data,function (select1Err, select1Result) {
                            callback({
                                code : updateResult.code,
                                info : "Login-Succesed",
                                message : "User sudah ada, data berhasil di-update",
                                data : select1Result.data
                            });
                        });
                });
            }else if(code == 1){
                userModel.insertUser(0,data, function (insertErr,insertResult) {
                        userModel.selectUser(0,data,function (select2Err, select2Result) {
                            callback({
                                code : insertResult.code,
                                info : "Login-Succesed",
                                message : "User berhasil didaftarkan",
                                data : select2Result.data
                            });
                        });
                });
            }else{
                callback(result);
            }
        });
    },

    userInsertProfile : function (accountId, data, callback) {
        userModel.updateUser(1,accountId,data,function (err,result) {
            if(result.code == 0){
                data["accountId"] = accountId;
                userModel.selectUser(1,data,function (selectErr, selectResult) {
                    callback({
                        code : result.code,
                        info : "Insert-Profile-Succes",
                        message : "User sudah ada, data berhasil di-update",
                        data : selectResult.data
                    });
                });
            }else{
                callback(result);
            }
        });
    }
};