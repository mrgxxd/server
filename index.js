'use strict';

/*
*   PLEASURRA API SERVER
*   https://api.pleasurra.com
*
*/

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//var routes = require('./routes');
var routes = express.Router();

var port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/v1', routes);

var userController = require('./controller/userController.js');
var experienceController = require('./controller/experienceController.js');

app.get('/', function (req, res) {
    var data = {
      message: "Welcome to Pleasurra API server.",
      info: "Full documentation on <https://bitbucket.org/pleasurradev/api>"
    };
    res.send(data)
});

// API for user sign in
routes.post('/users/login',function (req,res) {
    var data = req.body;
    userController.userLogin(data,function(result) {
        res.json(result);
    });
});

routes.put('/users/insertProfile/:accountId',function (req,res) {
    var accountId = req.params.accountId;
    var data = req.body;
    userController.userInsertProfile(accountId,data,function (result) {
        res.json(result);
    });
});

routes.get('/events/list',function (req,res) {
    experienceController.listExperience(function (result) {
        res.json(result);
    });
});

routes.post('/events/create',function (req,res) {
    var data = req.body;
    experienceController.createExperience(data,function (result) {
       res.json(result);
    });
});

app.listen(port);
