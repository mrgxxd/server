var pg = require('pg');
var co = require('co');
var pg1 = require('co-pg')(require('pg'));

var database = require('../config/moduleConfig.js');
var conString = database.connectionString;

var sql = "";
var param = "";

module.exports = {

    selectExperience : function (mode, data, callback) {
        var client = new pg.Client(conString);

        switch (mode){
            case 0:
                sql = "SELECT * FROM events";
                param = [];
                break;
        }

        client.connect(function (err) {
           if(err){
               callback(err, {
                   code : 3,
                   info : "Select-Failed",
                   message : "Tidak BIsa Konek ke Database",
                   data : err.toString()
               });
               return;
           }

            client.query(sql,param, function (err, result) {
                client.end();
                if (err) {
                    callback(err, {
                        code : 2,
                        info : "Select-Failed",
                        message : "Query tidak dapat dieksekusi",
                        data : err.toString()
                    });
                    return;
                }

                if (result.rows.length == 0){
                    callback(null, {
                        code : 1,
                        info : "Select-Failed",
                        message : "Data yang anda cari tidak ada",
                        data : null
                    });
                } else{
                    callback(null, {
                        code : 0,
                        info : "Select-Success",
                        message : "Data yang anda cari ada",
                        data : result.rows
                    });
                }
            });
        });
    },

    insertExperience : function (mode,data,callback) {
        var client = new pg.Client(conString);

        switch (mode){
            case 0:
                sql = "INSERT INTO events2" +
                    "(event_title,description,location,latitude,longitude,picture,account_id) " +
                    "VALUES($1,$2,$3,$4,$5,$6,$7)";
                param = [data.title,data.description,data.location,data.latitude,data.longitude,data.picture,data.accountId];
                break;
        }

        client.connect(function (err) {
            if(err){
                callback(err, {
                    code : 3,
                    info : "Insert-Failed",
                    message : "Tidak BIsa Konek ke Database",
                    data : err.toString()
                });
                return;
            }

            client.query(sql,param, function (err, result) {
                client.end();
                if (err) {
                    callback(err, {
                        code : 2,
                        info : "Insert-Failed",
                        message : "Query tidak dapat dieksekusi",
                        data : err.toString()
                    });
                    return;
                }

                if (result.rowCount == 0){
                    callback(null, {
                        code : 1,
                        info : "Insert-Failed",
                        message : "Gagal memasukan data event",
                        data : null
                    });
                } else if(result.rowCount > 0){
                    callback(null, {
                        code : 0,
                        info : "Insert-Success",
                        message : "Data event berhasil dimasukan ke database",
                        data : result
                    });
                }
            });
        });
    },

    updateExperience : function (params, callback) {

    },

    deleteExperience : function (params, callback) {

    }
};
