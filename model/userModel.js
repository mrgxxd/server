var pg = require('pg');

var database = require('../config/moduleConfig.js');
var conString = database.connectionString;

var sql = "";
var param = "";

module.exports = {
    //Function for selection data in table
    selectUser : function (mode,data,callback) {
        var client = new pg.Client(conString);

        switch (mode){
            case 0 :
                sql = "SELECT * FROM users WHERE phone_number= $1";
                param = [data.phoneNumber];
                break;
            case 1 :
                sql = "SELECT * FROM users WHERE account_id= $1";
                param = [data.accountId];
                break;
        }

        client.connect(function (err) {
            if(err){
                callback(err, {
                    code : 3,
                    info : "Select-Failed",
                    message : "Koneksi ke Database terputus",
                    data : err.toString()
                });
                return;
            }

            client.query(sql,param, function (err, result) {
                client.end();
                if (err) {
                    callback(err, {
                        code : 2,
                        info : "Select-Failed",
                        message : "Query tidak dapat dieksekusi",
                        data : err.toString()
                    });
                    return;
                }

                if (result.rows.length == 0){
                    callback(null, {
                        code : 1,
                        info : "Select-Failed",
                        message : "Data yang anda cari tidak ada",
                        data : null
                    });
                } else if (result.rows.length > 0){
                    callback(null, {
                        code : 0,
                        info : "Select-Success",
                        message : "Data yang anda cari ada",
                        data : result.rows
                    });
                }
            });
        });
    },

    //Function for insert into table
    insertUser : function (mode,data,callback) {
        var client = new pg.Client(conString);

        switch (mode){
            case 0 :
                sql = "INSERT INTO users(account_id,phone_number,token) VALUES($1,$2,$3)";
                param = [data.accountId,data.phoneNumber,data.token];
                break;
        }

        client.connect(function (err) {
            if(err){
                callback(err, {
                    code : 3,
                    info : "Insert-Failed",
                    message : "Koneksi ke Database terputus",
                    data : err.toString()
                });
                return;
            }

            client.query(sql,param, function (err, result) {
                client.end();
                if (err) {
                    callback(err, {
                        code : 2,
                        info : "Insert-Failed",
                        message : "Query tidak dapat dieksekusi",
                        data : err.toString()
                    });
                    return;
                }

                if (result.rowCount == 0){
                    callback(null, {
                        code : 1,
                        info : "Insert-Failed",
                        message : "Data gagal dimasukan ke dalam database",
                        data : null
                    });
                } else{
                    callback(null, {
                        code : 0,
                        info : "Insert-Success",
                        message : "Data user berhasil dimasukan",
                        data : result
                    });
                }
            });
        });
    },

    //Function for update data in table
    updateUser : function (mode,accountId,data,callback) {
        var client = new pg.Client(conString);

        switch (mode){
            case 0 :
                sql = "UPDATE users * SET account_id=$1 WHERE account_id= $2";
                param = [data.accountId,accountId];
                break;
            case 1 :
                sql = "UPDATE users * SET name=$1, email=$2 WHERE account_id= $3";
                param = [data.name,data.email,accountId];
                break;
        }

        client.connect(function (err) {
            if(err){
                callback(err, {
                    code : 3,
                    info : "Update-Failed",
                    message : "Koneksi ke Database terputus",
                    data : err.toString()
                });
                return;
            }

            client.query(sql,param, function (err, result) {
                client.end();
                if (err) {
                    callback(err, {
                        code : 2,
                        info : "Update-Failed",
                        message : "Query tidak dapat dieksekusi",
                        data : err.toString()
                    });
                    return;
                }

                if (result.rowCount == 0){
                    callback(null, {
                        code : 1,
                        info : "Update-Failed",
                        message : "Data gagal diperbaharui ke dalam database",
                        data : null
                    });
                } else if(result.rowCount > 0){
                    callback(null, {
                        code : 0,
                        info : "Update-Success",
                        message : "Data berhasil di update",
                        data : result
                    });
                }
            });
        });
    },

    //Function for delete data in table
    deleteUser : function (param,mode) {
        
    }

};


