'use strict'

const events = require('express').Router()

const all = require('./all')
const single = require('./single')

const userController = require('../../controller/userController')

// BASIC REST: https://en.wikipedia.org/wiki/Representational_state_transfer#Relationship_between_URL_and_HTTP_methods
events.use('/', all)
events.use('/:eventId', single)

// SPECIFIC METHODS GO BELOW

module.exports = events
