'use strict'

/*
*   PLEASURRA API ROUTES
*/

const routes = require('express').Router()
const users = require('./users')
const events = require('./events')

routes.use('/users', users)
routes.use('/events', events)

module.exports = routes
