'use strict'

const users = require('express').Router()

const all = require('./all')
const single = require('./single')

const userController = require('../../controller/userController')

// BASIC REST: https://en.wikipedia.org/wiki/Representational_state_transfer#Relationship_between_URL_and_HTTP_methods
users.use('/', all)
users.use('/:userId', single)

// LOGIN
users.post('/login', (req,res) => {
  var code = req.body.code
  var data = JSON.parse(req.body.data)
  userController.userSignUp(code,data, (err, result) => {
      res.send(result)
  })
})

users.post('/logout', (req, res) => {
  // TODO: create this endpoint
  let result = {}
  res.send(result)
})

module.exports = users
