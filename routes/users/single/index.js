'use strict'

const singleUser = require('express').Router({mergeParams: true})

// CREATE - NOT USED
singleUser.post('/', (req, res) => {
  // NOT USED
  let result = {}
  res.status(404).send(result)
})

// READ
singleUser.get('/', (req, res) => {
  // TODO: decide authentication method and then implement this
  let result = {}
  res.status(404).send(result)
})

// UPDATE
singleUser.put('/', (req, res) => {
  var code = req.body.code;
  var data = JSON.parse(req.body.data);
  userController.userAuth(code, data, (err,result) => {
     res.send(result);
  });
})

// DELETE
singleUser.delete('/', (req, res) => {
  // TODO: decide authentication method and then implement this
  let result = {}
  res.status(404).send(result)
})

module.exports = singleUser
